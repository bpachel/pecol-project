import http.client
import random
import time
from datetime import datetime

id_device = [5555555, 6666666, 7777777, 8888888, 9999999]
while (1):
    conn = http.client.HTTPConnection("localhost", 3000)
    for id in id_device:
        PM25 = random.randint(0, 20)
        PM10 = random.randint(15, 40)
        pressure = random.randint(980, 1010)
        temperature = random.randint(15, 25)
        humidity = random.randint(60, 80)

        payload = 'id_device='+str(id)+'&PM25='+str(PM25)+'&PM10='+str(PM10)+'&pressure='+str(pressure)+'&temperature='+str(temperature)+'&humidity='+str(humidity)
        headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
        }
        conn.request("POST", "/api/data/create", payload, headers)
        res = conn.getresponse()
        data = res.read()
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(str(current_time)+" - "+str(id)+":"+str(data.decode("utf-8")))
        time.sleep(1)
    time.sleep(60*20)
"""payload = 'id_device=9999999&PM25=12&PM10=21&pressure=1000&temperature=18&humidity=72'
headers = {
  'Content-Type': 'application/x-www-form-urlencoded'
}
conn.request("POST", "/api/data/create", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))
"""