import {Devices} from "../database/entity/Devices";
import {Data} from "../database/entity/Data";
import {getConnection} from "typeorm";

export interface IData{
    id_device: number,
    PM25: number,
    PM10: number,
    pressure: number,
    temperature: number,
    humidity: number
}

//dodawanie rekordu
export const createData = async(data: IData) => {
    try {
        const _newData = new Data();

        const _foundDevice = await Devices.findOne({where:{ id_device:data["id_device"]}})
        if(!_foundDevice){
            const _newDevice = new Devices();
            _newDevice["id_device"] = data["id_device"];
            await _newDevice.save();
        }
        _newData['devices'] = _foundDevice;

        if(data["PM25"]) _newData["PM25"] = data["PM25"];
        if(data["PM10"]) _newData["PM10"] = data["PM10"];
        if(data["pressure"]) _newData["pressure"] = data["pressure"];
        if(data["temperature"]) _newData["temperature"] = data["temperature"];
        if(data["humidity"]) _newData["humidity"] = data["humidity"];

        return await _newData.save();
    } catch (err){
        console.log(err);
        return ({"mess": "blad tworzenia"});
    }
}

//odczyt udostępnionych rekordów
export const getSharedData = async() => {
    try {
        const connection = getConnection();
        const getData = Data.createQueryBuilder("data")
            .innerJoin("data.devices", "device")
            .select([
                "device.address",
                "device.name",
                "data.id_device",
                "data.PM25",
                "data.PM10",
                "data.pressure",
                "data.humidity",
                "data.temperature"
            ])
            .where("device.shared = :sharedDevice", { sharedDevice: true })
            .orderBy("data.time", "DESC")
            .orderBy("data.id_device", "DESC")
            .getMany()
            return await getData.then((result)=>{
                var data = [];
                var id = [result[0].id_device]
                data.push(result[0])
                for(let i in result){
                    if(!id.find(element => element == result[i].id_device)){
                        id.push(result[i].id_device)
                        data.push(result[i])
                    }
                }
                return data;
            })

    } catch (err){
        console.log(err);
    }
}

//odczyt rekordu
export const getData = async(dataId?: number) => {
    try {

        if (dataId) {
            return await Data.findOne({
                where: {id_data: dataId}
            });
        } else {        // get all todos
            return await Data.find();
        }
    } catch (err){
        console.log(err);
    }
}

//odczyt rekordów
export const getDataDevices = async(deviceId?: number) => {
    try {
        if (deviceId) {
            return await Data.find({ where: { devices: deviceId } });
        } else {        // get all todos
            return {message: "brak id"};
        }
    } catch (err){
        console.log(err);
    }
}

//usuwanie rekordu
export const deleteData = async(dataId: number) => {
    try {
        const _foundData = await Data.findOne({ where: { id_data: dataId } });
        if (!_foundData) return { message: "not found" };
        return await _foundData.remove();
    } catch (err){
        console.log(err);
    }
}

//usuwanie rekordów
export const deleteDataDevices = async(deviceId: number) => {
    try {
        const _foundData = await Data.find({ where: { devices: deviceId} });
        if (!_foundData) return { message: "not found" };
        _foundData.forEach(function (record){
            record.remove();
        });
        return { message: "usunieto " + _foundData.length };
    } catch (err){
        console.log(err);
    }
}

//aktualizowanie rekoródw
export const updateData = async(data: {id_data: number} & IData) => {
    try {
        const _foundData = await Data.findOne({ where: { id_data: data["id_data"]} });
        if (!_foundData) return { message: "not found user" };

        if(data["PM25"]) _foundData["PM25"] = data["PM25"];
        if(data["PM10"]) _foundData["PM10"] = data["PM10"];
        if(data["pressure"]) _foundData["pressure"] = data["pressure"];
        if(data["temperature"]) _foundData["temperature"] = data["temperature"];
        if(data["humidity"]) _foundData["humidity"] = data["humidity"];

        return await _foundData.save();
    } catch (err){
        console.log(err);
    }
}