import {Users} from "../database/entity/Users";
import {UsersDetails} from "../database/entity/UsersDetails";

export interface IUsers{
    name: string,
    surname: string,
    email: string,
    password: string,
    enabled: boolean,
    cookies: string
}

//tworzenie użytkownika
export const createUser = async(user: IUsers) => {
    try {
        const _newUser = new Users();
        const _newUserDetails = new UsersDetails();

        _newUserDetails['name'] = user['name'];
        _newUserDetails['surname'] = user['surname'];
        await _newUserDetails.save();

        _newUser['usersDetails'] = _newUserDetails;
        if(user["email"])_newUser["email"] = user["email"];
        if(user["password"])_newUser["password"] = user["password"];
        if(user["enabled"])_newUser["enabled"] = user["enabled"];
        if(user["cookies"])_newUser["cookies"] = user["cookies"];

        return await _newUser.save()
    } catch (err){
        console.log(err);
    }
}

export const getUserByEmail = async(user: IUsers) => {
    try {
        const userData = Users.findOne({
            where:{email:user["email"]}
        });
        return userData;
    } catch (err){
        console.log(err);
    }
}

//odczyt użytkownika
export const getUser = async(userId?: number) => {
    try {

        if (userId) {
            return await Users.findOne({
                where: {id_user: userId },
            });
        } else {        // get all todos
            const users = await Users.find();
            return users;
        }
    } catch (err){
        console.log(err);
    }
}

//odczyt użytkownika
export const getUserName = async(userId: number) => {
    try {
        const _foundUser = await Users.findOne({ where: { id_user: userId} });
        return await UsersDetails.findOne({ where: { id_user_details: _foundUser['id_user_details']}});
    } catch (err){
        console.log(err);
    }
}

//usuwanie użytkownika
export const deleteUser = async(userId: number) => {
    try {
        const _foundUser = await Users.findOne({ where: { id_user: userId } });
        if (!_foundUser) return { message: "not found" };
        return await _foundUser.remove();
    } catch (err){
        console.log(err);
    }
}

//aktualizowanie użytkownika prawdopodobnie żle
export const updateUser = async(user: {id_user: number} & IUsers) => {
    try {
        const _foundUser = await Users.findOne({ where: { id_user: user["id_user"]} });
        const _foundUserDetails = await UsersDetails.findOne({ where: { id_user_details: _foundUser["id_user_details"]}});
        if (!_foundUser) return { message: "not found user" };
        if (!_foundUserDetails) return { message: "not found user details" };

        if(user["name"]) _foundUserDetails['name'] = user['name'];
        if(user["surname"])_foundUserDetails['surname'] = user['surname'];
        await _foundUserDetails.save();

        if(user["email"]) _foundUser["email"] = user["email"];
        if(user["password"]) _foundUser["password"] = user["password"];
        if(user["enabled"]) _foundUser["enabled"] = user["enabled"];
        if(user["cookies"]) _foundUser["cookies"] = user["cookies"];

        return await _foundUser.save();
    } catch (err){
        console.log(err);
    }
}