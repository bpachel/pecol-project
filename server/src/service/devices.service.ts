import {Devices} from "../database/entity/Devices";
import {Users} from "../database/entity/Users";

export interface IDevices{
    id_device: number,
    id_user: number,
    name: string,
    address: string,
    shared: boolean
}

//dodawanie urządzenia
export const createDevice = async(device: IDevices) => {
    try {
        const _newDevice = new Devices();
        if(device["id_device"]) _newDevice["id_device"] = device["id_device"];
        if(device["id_user"]) _newDevice['user'] = await Users.findOne({where:{ id_user:device["id_user"]}});
        if(device["name"]) _newDevice["name"] = device["name"];
        if(device["address"]) _newDevice["address"] = device["address"];
        if(device["shared"]) _newDevice["shared"] = device["shared"];

        return await _newDevice.save();
    } catch (err){
        console.log(err);
        return ({"mess": "blad tworzenia"});
    }
}

//odczyt urządzen
export const getDevice = async(userId?: number) => {
    try {

        if (userId) {
            return await Devices.find({
                where: {id_user: userId},
            });
        } else {        // get all todos
            return await Devices.find();
        }
    } catch (err){
        console.log(err);
    }
}

//usuwanie urządzenia
export const deleteDevice = async(deviceId: number) => {
    try {
        const _foundDevice = await Devices.findOne({ where: { id_device: deviceId } });
        if (!_foundDevice) return { message: "not found" };
        return await _foundDevice.remove();
    } catch (err){
        console.log(err);
    }
}

//aktualizowanie urządzenia
export const updateDevice = async(device: IDevices) => {
    try {
        const _foundDevice = await Devices.findOne({ where: { id_device: device["id_device"]} });
        if (!_foundDevice) return { mess: "not found device" };

        if(device["name"]) _foundDevice["name"] = device["name"];
        if(device["address"]) _foundDevice["address"] = device["address"];
        if(device["shared"]!=null) _foundDevice["shared"] = device["shared"];
        if(device["id_user"]) _foundDevice["user"] = await Users.findOne({where:{id_user: device["id_user"]}});

        return await _foundDevice.save();
    } catch (err){
        console.log(err);
    }
}