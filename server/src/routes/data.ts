import {DataController} from "../controller/data.controller";
import * as express from 'express';

const router = express.Router();

//używam
router.post('/create', (req, res, next) => {
    const data = new DataController();
    const promise = data.createData(req.body);
    promise.then((result)=>{
       res.json(result);
    });
});

router.get('/sharedData', (req, res, next)=>{
    const data = new DataController();
    //const tmp = data.getSharedData()

    const promise = data.getSharedData();
    promise.then((result)=>{
        res.json(result);
    });
})

router.get('/read', (req, res, next)=>{
    const data = new DataController();
    const promise = data.getData();
    promise.then((result) => {
        res.json(result);
    });
})

router.get('/read/:dataId', (req, res,next)=>{
    const data = new DataController();
    const promise = data.getDataWithId(parseInt(req.params.dataId,10));
    promise.then((result) => {
        res.json(result);
    });
})

router.get('/readAll/:deviceId', (req, res,next)=>{
    const data = new DataController();
    const promise = data.getDataDevices(parseInt(req.params.deviceId,10));
    promise.then((result) => {
        res.json(result);
    });
})

router.put('/update', (req, res,next) =>{
    const data = new DataController();
    const promise = data.updateData(req.body);
    promise.then((result) => {
        res.json(result);
    });
})

router.delete('/delete/:dataId', (req, res,next) =>{
    const data = new DataController();
    const promise = data.deleteData(parseInt(req.params.dataId,10));
    promise.then((result) => {
        res.json(result);
    });
})

router.delete('/deleteAll/:deviceId', (req, res,next) =>{
    const data = new DataController();
    const promise = data.deleteDataDevices(parseInt(req.params.deviceId,10));
    promise.then((result) => {
        res.json(result);
    });
})
export = router;
