import {DevicesController} from "../controller/devices.controller";
import * as express from 'express';
import {IGetUserAuthInfoRequest} from "../../definitionFile";
import * as jwt from "jsonwebtoken";

const router = express.Router();

const verifyJWT = (req, res, next)=>{
    const token = req.headers["x-access-token"]
    if(!token){
        res.send("Nie posiadasz tokenu");
    } else{
        jwt.verify(token, "pecol", (err, decoded)=>{
            if(err){
                res.json({auth: false})
            } else{
                req.userId = decoded.id;
                next();
            }
        })
    }
}

router.post('/create', (req, res, next) => {
    const device = new DevicesController();
    const promise = device.createDevice(req.body);
    promise.then((result) => {
        res.json(result);
    });
});

//używam
router.put('/update', verifyJWT, (req: IGetUserAuthInfoRequest, res,next) =>{
    const device = new DevicesController();
    const data = {
        id_user: req.userId,
        id_device: req.body.id_device,
        name: req.body.name,
        address: req.body.address,
        shared: (req.body.shared==="true"),
    };
    //console.log(data);
    const promise = device.updateDevice(data);
    promise.then((result)=>{
        res.json(result);
    });
})

//używam
router.get('/get', verifyJWT, (req: IGetUserAuthInfoRequest, res,next)=>{
    const device = new DevicesController();
    const promise = device.getDeviceWithId(req.userId);
    promise.then((result) => {
        res.json(result);
    });
})

router.get('/getAll', (req, res, next)=>{
    const device = new DevicesController();
    const promise = device.getDevice();
    promise.then((result) => {
        res.json(result);
    });
})

router.delete('/delete/:deviceId', (req, res,next) =>{
    const device = new DevicesController();
    const promise = device.deleteDevice(parseInt(req.params.deviceId,10));
    promise.then((result) => {
        res.json(result);
    });
})
export = router;

