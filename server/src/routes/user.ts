import { Controller, ValidationService, FieldErrors, ValidateError, TsoaRoute } from 'tsoa';
import {UsersController} from "../controller/users.controller";
import {IGetUserAuthInfoRequest} from "../../definitionFile";
import * as express from 'express';
import * as jwt from 'jsonwebtoken'
import * as bcrypt from 'bcrypt';
const saltRound = 10;
const router = express.Router();

//używam
const verifyJWT = (req, res, next)=>{
    const token = req.headers["x-access-token"]
    if(!token){
        res.send("Nie posiadasz tokenu");
    } else{
        jwt.verify(token, "pecol", (err, decoded)=>{
            if(err){
                res.json({auth: false})
            } else{
                req.userId = decoded.id;
                next();
            }
        })
    }
}
//używam
router.post('/create', (req, res, next) => {
    const user = new UsersController();
    //bcrypt.hash(req.body)
    bcrypt.hash(req.body.password, saltRound, (err, hash)=>{
        if(err){
            console.log(err)
        }
        req.body.password = hash;
        const promise = user.createUser(req.body);
        promise.then((result) =>{
            if(result!=undefined) {
                const token = jwt.sign({id: result.id_user}, "pecol", {
                    expiresIn: 60 * 60,
                });
                res.json({auth: true, token: token, user: result});
            }
            else res.json({auth:false, mess: "Błąd tworzenia użytkownika"});
        })
    });

});
//używam
router.post('/getUser', (req, res, next) => {
    const user = new UsersController();
    const promise = user.getUserByEmail(req.body);
    promise.then((result) =>{
        if(result!=undefined) {
            bcrypt.compare(req.body.password, result.password, (error, response)=>{
                if(response){
                    res.cookie('userId', result.id_user, {
                        maxAge: 60 * 60 * 1000,
                        httpOnly: true,
                        secure: true,
                        sameSite: true,
                    })
                    const token = jwt.sign({id:result.id_user}, "pecol", {
                        expiresIn: 60*60,
                    });

                    res.json({auth: true, token: token, user: result});
                }
                else res.json({mess: "Wprowadzono nieprawidłowe hasło"});
            });
        }
        else res.json({mess: "Nie znaleziono użytkownika"});
    })
});

//używam
router.get('/isUserAuth', verifyJWT,(req: IGetUserAuthInfoRequest, res)=>{
    const user = new UsersController();
    const promise = user.getUserName(req.userId);
    promise.then((result)=>{
        if(result!=undefined) {
            res.json({auth: true, user:result});
        }
        else res.json({auth: false});
    })
});

//NIE
router.get('/getUserName', (req, res, next) => {
    console.log(req.cookies.userId);
    if(req.cookies.userId!=undefined){
        const user = new UsersController();
        const promise = user.getUserName(req.cookies.userId);
        promise.then((result) =>{
            if(result!=undefined) {
                res.json({auth: true, user:result});
            }
            else res.json({mess: "wrong data"});
        })
    }else{
        res.json({auth: false});
    }
});

router.get('/read', (req, res, next)=>{
    const user = new UsersController();
    const promise = user.getUser();
    promise.then((result) =>{
        res.json(result);
    })
})

router.get('/read/:userId', (req, res,next)=>{
    const user = new UsersController();
    const promise = user.getUserWithId(parseInt(req.params.userId,10));
    promise.then((result) =>{
        res.json(result);
    })
})

router.put('/update', (req, res,next) =>{
    const user = new UsersController();
    const promise = user.updateUser(req.body);
    promise.then((result) =>{
        res.json(result);
    })
})

router.delete('/delete/:userId', (req, res,next) =>{
    const user = new UsersController();
    const promise = user.deleteUser(parseInt(req.params.userId,10));
    promise.then((result) =>{
        res.json(result);
    })
})
export = router;






