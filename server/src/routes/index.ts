import * as express from "express";

import * as userRouter from "./user";
import * as deviceRouter from "./device";
import * as dataRouter from "./data";
import {Controller} from "tsoa";

export const router = express.Router();
router.use('/api/user', userRouter);
router.use('/api/device', deviceRouter);
router.use('/api/data', dataRouter);


//export = router;