import { Body, Controller, Delete, Get, Path, Post, Put, Query, Route, Tags } from 'tsoa';
import {IDevices, createDevice, deleteDevice, getDevice, updateDevice} from "../service/devices.service";


@Tags('Devices')
@Route('/api/device')
export class DevicesController extends Controller {

    // CREATE CONTROLLER
    @Post('/create')
    public async createDevice(@Body() device: IDevices) {
        return createDevice(device)
    }

    // READ CONTROLLER
    @Get('/getAll')
    public async getDevice() {
        return getDevice()
    }

    @Get('/get')
    public async getDeviceWithId(userId: number) {
        return getDevice(userId)
    }

    // UPDATE CONTROLLER
    @Put('/update')
    public async updateDevice(@Body() device: { id_device: number } & IDevices) {
        return updateDevice(device)
    }

    // DELETE CONTROLLER
    @Delete('/delete/{deviceId}')
    public async deleteDevice(@Path('deviceId') deviceId: number) {
        return deleteDevice(deviceId)
    }

}