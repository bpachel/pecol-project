import { Body, Controller, Delete, Get, Path, Post, Put, Query, Route, Tags } from 'tsoa';
import{IData, createData, deleteData, deleteDataDevices, getData, getDataDevices, updateData, getSharedData} from "../service/data.service";


@Tags('Data')
@Route('/api/data')
export class DataController extends Controller {

    // CREATE CONTROLLER
    @Post('/create')
    public async createData(@Body() data: IData) {
        return createData(data);
    }

    // READ CONTROLLER
    @Get('/sharedData')
    public async getSharedData() {
        return getSharedData();
    }

    @Get('/read')
    public async getData() {
        return getData();
    }

    @Get('/read/{dataId}')
    public async getDataWithId(@Path('dataId') dataId: number) {
        return getData(dataId);
    }

    @Get('/readAll/{deviceId}')
    public async getDataDevices(@Path('deviceId') deviceId: number){
        return getDataDevices(deviceId);
    }

    // UPDATE CONTROLLER
    @Put('/update')
    public async updateData(@Body() data: { id_data: number } & IData) {
        return updateData(data);
    }

    // DELETE CONTROLLER
    @Delete('/delete/{dataId}')
    public async deleteData(@Path('dataId') dataId: number) {
        return deleteData(dataId);
    }

    @Delete('/deleteAll/{deviceId}')
    public async deleteDataDevices(@Path('deviceId') deviceId: number){
        return deleteDataDevices(deviceId);
    }

}