import { Body, Controller, Delete, Get, Path, Post, Put, Query, Route, Tags } from 'tsoa';
import { IUsers, createUser, getUserByEmail, deleteUser, getUser, getUserName, updateUser} from "../service/users.service";

@Tags('Users')
@Route('/api/user')
export class UsersController extends Controller {

    // CREATE CONTROLLER
    @Post('/create')
    public async createUser(@Body() user: IUsers) {
        return createUser(user)
    }

    @Post('/getUser')
    public async getUserByEmail(@Body() user: IUsers) {
        return getUserByEmail(user)
    }

    // DELETE CONTROLLER
    @Get('/getUserName/{userId}')
    public async getUserName(@Path('userId') userId: number) {
        return getUserName(userId)
    }

    // READ CONTROLLER
    @Get('/read')
    public async getUser() {
        return getUser()
    }

    @Get('/read/{userId}')
    public async getUserWithId(@Path('userId') userId: number) {
        return getUser(userId)
    }

    // UPDATE CONTROLLER
    @Put('/update')
    public async updateUser(@Body() user: { id_user: number } & IUsers) {
        return updateUser(user)
    }

    // DELETE CONTROLLER
    @Delete('/delete/{userId}')
    public async deleteUser(@Path('userId') userId: number) {
        return deleteUser(userId)
    }

}