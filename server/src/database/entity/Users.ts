import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn, OneToMany, BaseEntity} from "typeorm";
import {UsersDetails} from "./UsersDetails";
import {Devices} from "./Devices";

@Entity({name: "users"})
export class Users extends BaseEntity{

    @PrimaryGeneratedColumn()
    id_user: number;

    @Column("varchar", { length: 50, unique: true })
    email: string;

    @Column("varchar", { length: 100 })
    password: string;

    @Column({default: false})
    enabled: boolean;

    @Column("varchar", { length: 50, unique: true, nullable: true})
    cookies: string;

    @Column()
    id_user_details: number;

    @OneToOne(()=>UsersDetails, usersDetails =>usersDetails.id_user_details, {
        onDelete: "CASCADE",
        onUpdate: "CASCADE"
    })
    @JoinColumn({name: "id_user_details"})
    usersDetails: UsersDetails;

    @OneToMany(() => Devices, devices => devices.id_device)
    devices: Devices[];
}
