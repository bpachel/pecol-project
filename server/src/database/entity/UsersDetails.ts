import {Entity, Column, PrimaryColumn, BaseEntity, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "users_details"})
export class UsersDetails extends BaseEntity{

    @PrimaryGeneratedColumn()
    id_user_details: number;

    @Column("varchar", { length: 50 })
    name: string;

    @Column("varchar", { length: 50 })
    surname: string;
}
