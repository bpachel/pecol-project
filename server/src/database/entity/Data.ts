import {Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, JoinColumn, BaseEntity} from "typeorm";
import {Devices} from "./Devices";

@Entity({ name:"data"})
export class Data extends BaseEntity{

    @PrimaryGeneratedColumn()
    id_data: number;

    @Column()
    PM25: number

    @Column()
    PM10: number;

    @Column()
    pressure: number;

    @Column()
    temperature: number;

    @Column()
    humidity: number;

    @Column({nullable: true})
    id_device: number;

    @CreateDateColumn()
    time;

    @ManyToOne(()=>Devices, devices => devices.id_device,{
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
    })
    @JoinColumn({name: "id_device"})
    devices: Devices;

}
