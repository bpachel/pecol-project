import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn,
    OneToMany,
    BaseEntity,
    PrimaryColumn
} from "typeorm";
import {Users} from "./Users";
import {Data} from "./Data";

@Entity({name: "devices"})
export class Devices extends BaseEntity{

    @PrimaryColumn()
    id_device: number;

    @Column("varchar", { length: 50 , default: "---"})
    name: string;

    @Column("varchar", { length: 50 , default: "---" })
    address: string;

    @Column({default: false})
    shared: boolean;

    @Column({nullable: true})
    id_user: number;

    @ManyToOne(()=>Users, users => users.id_user,{
        onDelete: 'CASCADE',
        onUpdate: "CASCADE",
        nullable: true
    })
    @JoinColumn({name: "id_user"})
    user: Users;

    @OneToMany(() => Data, data => data.id_data)
    Data: Data[];
}
