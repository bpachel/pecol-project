import "reflect-metadata";
import * as express from "express";
import * as path from 'path';
import * as hbs from  'express-handlebars';
import {router} from "./src/routes/index";
import * as cors from 'cors';
import * as cookieParser from 'cookie-parser';
import * as session from 'express-session';

import * as bodyparser from 'body-parser';
import * as swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from './swagger.json'
import {createConnection} from "typeorm";

const app = express();

app.engine('hbs', hbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: __dirname + '/views/layouts/'}));
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'hbs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(
    session({
        //key: "userId",
        name: "userId",
        secret: "pecol",
        resave: false,
        saveUninitialized: false,
        unset: 'keep',
        cookie:{
            maxAge: 60*60*4,
            expires: new Date(),
            //secure: false
        }
    })
);
app.use(cors({
    origin: ["http://localhost:8080"],
    methods: ["GET", "POST"],
    credentials: true,
}));
app.use(cookieParser('pecol'));
app.use(bodyparser.urlencoded({
    extended: true
}));
//app.use(express.json);
createConnection().then(async () =>{
    console.log('Database connected..');
    app.listen(3000, async () => {
        console.log('Servers is listening at http://localhost:3000/');
        app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    });
}).catch(err => console.log(err));

app.use("/", router);


