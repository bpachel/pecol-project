import React from 'react';
import "../styles/main.css";
import logo from '../images/logo2.svg';
const IndexPage = () => {

    return (
        <>
            <div className="base-container">
                <img id="logo" src={logo}></img>

                    <div id="text-container" className="welcome">

                        <h1 className="welcome">Dołącz do naszej społeczności<br/>
                            i zadbaj o świadomość jakości powietrza.
                        </h1>
                        <div id="button-container" className="welcome">
                            <a href="register" className="logButton">
                                Utwórz konto
                            </a>
                            <a href="login" className="logButton">
                                Zaloguj się
                            </a>
                        </div>
                    </div>
                    <div id="empty"></div>
            </div>
        </>
    );
}

export default IndexPage;