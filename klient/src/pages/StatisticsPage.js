import React, {useEffect, useState} from 'react';
import "../styles/afterLogin.css";

import stat1 from '../images/stat1.png';
import stat2 from '../images/stat2.png';
import graph from '../images/graph.png';

import Navigation from '../layouts/Navigation';
import Axios from "axios";
import {Redirect} from "react-router-dom";

const StatisticsPage = () => {
    const [loginStatus, setLoginStatus] = useState(true);
    const [namesList, setNamesList] = useState([]);
    const list = namesList.map(nameDev => (
        <option key={nameDev}>{nameDev}</option>
    ))
    var names = [];
    const setDataAndNames = () => {
        Axios.get("api/device/get", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            const tmp = response.data;
            for(let i in tmp){
                names.push(tmp[i].name);
            }
            setNamesList(names);
        })
    }

    useEffect(() => {
        Axios.get("api/user/isUserAuth", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            if(response.data.auth==true) setLoginStatus(true);
            else {
                alert("Musisz się zalogować!");
                setLoginStatus(false);
            }
        })
        setDataAndNames();
    }, []);

    if(!loginStatus) return <Redirect to="/"/>

    return (
        <>
            <Navigation />
            <div className="content-container">
                <div className="choice-device-container">
                    <p><i className="fas fa-battery-three-quarters"></i><b> 3,6V</b></p>
                    <select className="stat">
                        {list}
                    </select>
                </div>
                <div className="data-container">
                    <div className="stat-container">
                        <img id="stat1" src={stat1}></img>
                        <img id="stat2" src={stat2}></img>
                    </div>
                    <img id="graph" src={graph}></img>
                </div>

            </div>
        </>
    );
}

export default StatisticsPage;