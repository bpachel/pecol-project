import React, {useEffect, useState} from 'react';
import "../styles/afterLogin.css";
import mapa from '../images/mapa.png';
import Navigation from '../layouts/Navigation';
import Axios from "axios";
import {Redirect} from "react-router-dom";
import LazyLoad from 'react-lazyload';
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'

const MainPage = () => {

    const [loginStatus, setLoginStatus] = useState(true);
    const [data, setData] = useState([]);
    const mainCoordinates = [52.215933, 19.134422];

    const dataArray = []
    const getData = () =>{
        Axios.get("api/data/sharedData", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            for(let i in response.data){
                dataArray.push([
                    response.data[i].id_device,
                    response.data[i].devices.name,
                    response.data[i].devices.address,
                    response.data[i].PM25,
                    response.data[i].PM10,
                    response.data[i].pressure,
                    response.data[i].temperature,
                    response.data[i].humidity
                    ])
            }
            setData(dataArray)
            setTimeout(()=>console.log(data),3000)
        })
    }

    const toArray = (text) =>{
        var tmp = []
        var sub = text.split(", ")
        for(let i in sub){
            tmp.push(sub[i])
        }
        return tmp
    }

    const addMarker = data.map(dataDet => (
            <Marker key={dataDet[0]} position={toArray( dataDet[2])}>
                <Popup>
                    Nazwa: {dataDet[1]}
                    <br /> PM2.5: {dataDet[3]}
                    <br /> PM10: {dataDet[4]}
                    <br /> pressure: {dataDet[5]}
                    <br /> temperature: {dataDet[6]}
                    <br /> humidity: {dataDet[7]}
                </Popup>
            </Marker>
        ))

    useEffect(() => {
        Axios.get("api/user/isUserAuth", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            if(response.data.auth==true) setLoginStatus(true);
            else {
                alert("Musisz się zalogować!");
                setLoginStatus(false);
            }
        })
        getData();
    }, []);

    if(!loginStatus) return <Redirect to="/"/>
    return (
        <>
            <Navigation />

                <div id="mapid">
                    <LazyLoad>
                        <MapContainer center={mainCoordinates} zoom={6} scrollWheelZoom={true}>
                            <TileLayer
                                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                            />
                            {addMarker}
                        </MapContainer>
                    </LazyLoad>
                </div>
        </>
    );
}

export default MainPage;