import React, {useEffect, useState} from 'react';
import "../styles/afterLogin.css";
import Navigation from '../layouts/Navigation';
import qs from "querystring";
import Axios from "axios";
import {Redirect, useLocation} from "react-router-dom";

const SettingsPage = () => {

    var names = [];
    const [loginStatus, setLoginStatus] = useState(true);

    const [deviceNumber, setDeviceNumber] = useState("");
    const [addressDevice, setAddress] = useState("");
    const [nameDevice, setName] = useState("");
    const [sharedDevice, setShared] = useState(false);

    const [namesList, setNamesList] = useState([]);
    const [data, setData] = useState();

    const list = namesList.map(nameDev => (
        <option key={nameDev}>{nameDev}</option>
    ))
    const addDevice = (e) => {
        e.preventDefault();
        setShared("");
        setName("");
        setAddress("");
        console.log(nameDevice);
        updateDataDevice();
    }
    const updateDevice = (event) => {
        //TODO po nazwie sam wprowadzał nr urządzenia
        event.preventDefault();
        updateDataDevice();
    }

    const updateDataDevice = () => {
        if(addressDevice){
            Axios.defaults.withCredentials = false;
            Axios.get('https://us1.locationiq.com/v1/search.php?key=pk.fc66782b74491d4d61c8adacf1fe901e&q='+encodeURI(addressDevice)+'&limit=1&accept-language=pl&countrycodes=pl&format=json', {
                headers: {
                },
            }).then((response)=>{
                const coordinates = (response.data[0].lat.toString()+", "+response.data[0].lon.toString());
                const device = qs.stringify({
                    id_device: deviceNumber,
                    name: nameDevice,
                    address: coordinates,
                    shared: sharedDevice
                });
                console.log(sharedDevice)
                Axios.put("api/device/update", device, {
                    headers: {
                        "x-access-token": localStorage.getItem("token"),
                    },
                }).then((response) => {
                    if(response.data.mess) alert("Błąd... " + response.data.mess);
                    else alert("Dodano urządzenie");
                    console.log(response);
                })
            })
        }else{
            const device = qs.stringify({
                id_device: deviceNumber,
                name: nameDevice,
                address: addressDevice,
                shared: sharedDevice
            });
            console.log(sharedDevice)
            Axios.put("api/device/update", device, {
                headers: {
                    "x-access-token": localStorage.getItem("token"),
                },
            }).then((response) => {
                if(response.data.mess) alert("Błąd... " + response.data.mess);
                else alert("Dodano urządzenie");
                console.log(response);
            })
        }
    }

    const setDataAndNames = () => {
        Axios.get("api/device/get", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            const tmp = response.data;
            for(let i in tmp){
                names.push(tmp[i].name);
            }
            setData(tmp);
            setNamesList(names);
        })
    }

    const inputData = (e) =>{
        for (let i in data){
            if(data[i].name == e.target.value){
                setName(data[i].name);
                setAddress(data[i].address);
                setShared(data[i].shared);
                setDeviceNumber(data[i].id_device);
            }
        }
        const tmp = document.getElementsByTagName("input");
        for(let i = 0; i<tmp.length; i++){
            tmp[i].value = "";
        }
    }

    //hook
    useEffect(() => {
        Axios.get("api/user/isUserAuth", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            if(response.data.auth==true) setLoginStatus(true);
            else {
                alert("Musisz się zalogować!");
                setLoginStatus(false);
            }
        })
        setDataAndNames()
    }, []);

    //render
    if(!loginStatus) return <Redirect to="/"/>
    return (
        <>
            <Navigation />
            <div className="settings-container">
                <div className="choice-device-container">
                    <p className="select-device">
                        Wybierz urządzenie do edycji:
                        <select onChange={(e)=>{inputData(e)}}>
                            {list}
                        </select>
                    </p>
                </div>
                <div className="form-container">
                    <form className="add" onSubmit={(e)=>{addDevice(e)}}>
                        <div className="input-container">
                            <p className="addDevice">Numer urządzenia</p>
                            <input name="ID" type="text" placeholder={deviceNumber} onChange={(e)=>{setDeviceNumber(e.target.value)}}/>
                        </div>
                        <button type="submit" className="addButton">Dodaj urządzenie</button>
                    </form>
                    <form className="set" onSubmit={(event)=>{updateDevice(event)}}>
                        <div className="input-container">
                            <p className="setDevice">Nazwa urządzenia</p>
                            <input name="name" type="text" placeholder={nameDevice} onChange={(e)=>{setName(e.target.value)}}/>
                        </div>
                        <div className="input-container">
                            <p className="setDevice">Adres urządzenia</p>
                            <input name="adress" type="text" placeholder={addressDevice} onChange={(e)=>{setAddress(e.target.value)}}/>
                        </div>

                        <div>
                            <input type="checkbox" id="shared" name="shared" onChange={(e)=>{setShared(e.target.checked)}}/>
                            <label htmlFor="share">Udostępnij dane na mapie</label>
                        </div>
                        <button type="submit" className="addButton">Zapisz</button>
                    </form>
                </div>
                <div id="empty"></div>
            </div>

        </>
    );
}

export default SettingsPage;