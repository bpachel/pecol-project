import React, {useEffect, useState } from 'react';
import {Redirect, Prompt} from "react-router-dom";
import "../styles/main.css";
import logo from '../images/logo2.svg';
import qs from "querystring";
import Axios from "axios";
import MainPage from "./MainPage";
import {render} from "@testing-library/react";
Axios.defaults.withCredentials = true;

export default function LoginPage(){
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [mess, setMess] = useState("");
    const [loginStatus, setLoginStatus] = useState(false);

    const login = () =>{
        const urlencoded = qs.stringify({
            email: email,
            password: password
        });
        Axios({
            method: 'post',
            url: 'api/user/getUser',
            data: urlencoded,

            headers:{
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((response)=> {
            console.log(response);
            if (response.data.mess) {
                alert(response.data.mess);
                setMess("Złe dane");
            } else {
                localStorage.setItem("token", response.data.token);
                setLoginStatus(true);
                //return <Redirect to="/main"/>
            }
        });
    };

    useEffect(() => {

        Axios.get("api/user/isUserAuth", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            console.log(response);
            if(response.data.auth==true) {
                alert("Zalogowano automatycznie...");
                setLoginStatus(true);
            }
        })
    }, []);

    if(loginStatus) return <Redirect to="/main"/>
    return (
        <>
            <div className="base-container">
                <img id="logo" src={logo}/>
                <div id="text-container" className="login">
                    <h1>Zaloguj się</h1>
                    <div id="form" className="login">
                        <div className="input-container">
                            <p className="login">E-mail</p>
                            <input name="email" type="text" placeholder="" onChange={(e)=>{setEmail(e.target.value)}}/>
                        </div>

                        <div className="input-container">
                            <p className="login">Hasło</p>
                            <input name="password" type="password" placeholder="" onChange={(e)=>{setPassword(e.target.value)}}/>
                        </div>
                        <button onClick={login}>Zaloguj</button>
                    </div>
                </div>
                <div id="empty"/>
            </div>
        </>
    );

}
