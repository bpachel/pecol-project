import React, {useEffect, useState} from 'react';
import "../styles/main.css";
import Axios from 'axios';
import qs from 'querystring';
import logo from '../images/logo2.svg';
import {Route, Redirect} from "react-router-dom";


export default function RegisterPage(){

    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [mess, setMess] = useState("");
    const [loginStatus, setLoginStatus] = useState(false);


    const register = () =>{
        const urlencoded = qs.stringify({
            name: name,
            surname: surname,
            email: email,
            password: password
        });
        Axios({
            method: 'post',
            url: 'api/user/create',
            data: urlencoded,

            headers:{
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then((response)=> {
            if (response.data.mess) {
                setMess(response.data.mess);
            } else {
                localStorage.setItem("token", response.data.token)
                alert("Zarejestrowano");
                setLoginStatus(true);
                console.log(response);
            }
        });
    }

    useEffect(() => {
        Axios.get("api/user/isUserAuth", {
            headers: {
                "x-access-token": localStorage.getItem("token"),
            },
        }).then((response)=>{
            if(response.data.auth==true) {
                alert("Zalogowano automatycznie...");
                setLoginStatus(true);
            }
        })
    }, []);

    const handleEmail = (e) =>{
        setEmail(e.target.value);
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        setTimeout(()=>{
            if(!re.test(e.target.value)){
                setMess("Adres email nie poprawny!");
            }
            else{
               setMess("");
            }
        }, 2000);
    }
    const handlePassword = (e) =>{
        setTimeout(()=>{
            if(e.target.value!=password){
                setMess("Hasła nie zgadzają się!");
            }
            else{
                setMess("");
            }
        }, 4000);
    }

    if(loginStatus) return <Redirect to="/main"/>
    return (
        <>
            <div className="base-container">
                <img id="logo" src={logo}/>
                <div id="text-container" className="register">
                    <h1 id="mess">Zarejestruj się<br></br><span class="red">{mess}</span></h1>
                    <div id="form" className="register">
                        <div className="input-container">
                            <p className="register">Imię</p>
                            <input name="name" type="text" placeholder="" onChange={(e)=>{setName(e.target.value)}}/>
                        </div>

                        <div className="input-container">
                            <p className="register">Nazwisko</p>
                            <input name="surname" type="text" placeholder="" onChange={(e)=>{setSurname(e.target.value)}}/>
                        </div>
                        <div className="input-container">
                            <p className="register">E-mail</p>
                            <input name="email" type="text" placeholder="" onChange={(e)=>handleEmail(e)}/>
                        </div>
                        <div className="input-container">
                            <p className="register">Hasło</p>
                            <input name="pass" type="password" placeholder="" onChange={(e)=>{setPassword(e.target.value)}}/>
                        </div>

                        <div className="input-container">
                            <p className="register">Powtórz hasło</p>
                            <input name="passConf" type="password" placeholder="" onChange={(e)=>{handlePassword(e)}}/>
                        </div>

                        <button onClick={register} >Zarejestruj</button>
                    </div>
                </div>
                <div id="empty"></div>
            </div>
        </>
    );
}