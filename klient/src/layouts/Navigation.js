import React from 'react';
import { NavLink } from 'react-router-dom';

import logo from '../images/logo2.svg';
import "../styles/afterLogin.css";

const list = [
    { name: "map", path: "/main", exact: true },
    { name: "statistics", path: "/statistics" },
    { name: "settings", path: "/settings" },
    { name: "logout", path: "/" },
]

const logout = () => {
    localStorage.removeItem("token");
}

const Navigation = () => {
  return (
      <nav>
          <div id="up">
              <NavLink to={list[0].path} exact={list[0].exact ? list[0].exact : false}>
                      <img id="logoNav" src={logo}/>
              </NavLink>
              <NavLink id="main" to={list[0].path} exact={list[0].exact ? list[0].exact : false}>
                  <div className="icon-nav-container">
                      <i className="fas fa-map-marker-alt"></i>
                  </div>
              </NavLink>
              <NavLink id="statistics" to={list[1].path} exact={list[1].exact ? list[1].exact : false}>
                  <div className="icon-nav-container">
                      <i className="fas fa-chart-area"></i>
                  </div>
              </NavLink>
              <NavLink id="settings" to={list[2].path} exact={list[2].exact ? list[2].exact : false}>
                  <div className="icon-nav-container">
                      <i className="fas fa-cog"></i>
                  </div>
              </NavLink>
          </div>

          <div id="down">
              <NavLink id="index" to={list[3].path} exact={list[3].exact ? list[3].exact : false }>
                  <div className="icon-nav-container">
                      <i className="fas fa-sign-out-alt" onClick={logout}></i>
                  </div>
              </NavLink>
          </div>
      </nav>
  );
}

export default Navigation;