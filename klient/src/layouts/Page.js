import React from 'react';
import { Route, Switch } from 'react-router-dom';

import MainPage from '../pages/MainPage';
import SettingsPage from '../pages/SettingsPage';
import StatisticsPage from '../pages/StatisticsPage';
import IndexPage from '../pages/IndexPage';
import RegisterPage from '../pages/RegisterPage';
import LoginPage from '../pages/LoginPage';


const Page = () => {
  return (
    <>
      <Switch>
        <Route path="/main" exact component={MainPage} />
        <Route path="/settings" exact component={SettingsPage} />
        <Route path="/statistics" exact component={StatisticsPage} />
        <Route path="/" exact component={IndexPage} />
        <Route path="/register" exact component={RegisterPage} />
        <Route path="/login" exact component={LoginPage} />
      </Switch>
    </>
  );
}

export default Page;